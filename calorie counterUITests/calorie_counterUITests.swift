//
//  calorie_counterUITests.swift
//  calorie counterUITests
//
//  Created by Tony Lambropoulos on 3/17/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import XCTest

class calorie_counterUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func addMeal(){
        
    }
    
    func testViewDaily(){
        
    }
    
    func testEditMeal(){
        
    }
    
    func testChangeUser(){
        
    }
    
    func testAddUser(){
        
    }
    
    func testDeleteUser(){
    
    }
    
    func testChangeCalorieTarget(){
        
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

}
