# To prepare the iOS app
### Ensure you have cocoapods
```
gem install cocoapods
```

### In the root directory of the repository install the Pod dependencies
```
pod install
```

# Installation To Run API Locally
### Setup a virtual environment for API
*Note: Naming the virtualenv "env" and placing it in the "zappa_api directory of the repository is necessary for packaging the app.*
```bash
cd zappa_api
virtualenv -p python3 env
source env/bin/activate

```

### Install the required python packages
*Note: Ensure the virtual environment created above is activated*
```
pip install -r requirements.txt
```

### Initialize local database and seed with roles
```
python init_db.py
```
### Switch base URL in Constants file to your local IP

### Run the Flask run file
```
cd zappa_api
python run.py
```

### Start the iOS app and create an account
And you should be ready to go!

### To deploy the Flask API
1. Ensure you are in your virtualenv environment with your requirements installed
2. Create an AWS account
3. Add your access key and secret key to your bash environment (AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY respectively)
4. The first deployment run
```
zappa deploy dev
```
5. And to update 
```
zappa update dev
```
Note that you will need a database connection to connnect to (which gets set with the "DATABASE_URL" env variable).  For this first setup, a micro instance of RDS was launched and the DATABASE_URL param was set as an AWS Lambda environment variable


# Limitations
- Users with higher priviledges can't change the role of lower level users yet.  The only way to create higher level users as of right now is through the command line
- Unit/E2E Tests haven't been fleshed out
- Currently using basic authentication
- There are no email registration or forgot password flows at the moment
- AWS API Gateway doesn't allow headers so had to use query parameters to pass credentials
- Not using any form of caching on table data
- Not using any form of pagination on table data
- Only using basic UI elements
- Error messaging needs a lot of improvement
- Code has a bit of redundancy
- Have done the majority of testing on iOS simulator
- String literals are used in several places to reference roles.  A bit fragile
- Meal query page is very clunky