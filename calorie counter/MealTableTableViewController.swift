//
//  MealTableTableViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/17/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class MealTableTableViewController: UITableViewController {
    var meals : [Meal] = []
    var startTimeString : String?
    var endTimeString : String?
    var startDateString : String?
    var endDateString :String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.meals = []
        APIClient.Fetch(sts: startTimeString, ets: endTimeString, sds: startDateString, eds: endDateString){ result in
            switch result {
            case .success(let mealsResponse):
                for mealResponse in mealsResponse.meals{
                    self.meals.append(mealResponse.meal)
                }
                
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        if let userId = User.GetUserMimicked() {
            let title = self.navigationItem.title
            self.navigationItem.title = title! + " (Mimicked: \(userId))"
        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 1 {
            return meals.count
        } else {
            return 1
        }
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mealCellIdentifier", for: indexPath)

        // Configure the cell...
        if indexPath.section == 1 {
            cell.textLabel?.text = meals[indexPath.row].name
            cell.detailTextLabel?.text = String(meals[indexPath.row].calories)
        } else {
            var totCals : Int32 = 0
            for meal in self.meals {totCals += meal.calories}
            cell.textLabel?.text = "Total Calories: " + String(totCals)
            cell.detailTextLabel?.text = ""
        }


        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
            let alert = UIAlertController(title: "", message: "Edit Meal", preferredStyle: .alert)
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Name"
                textField.text = self.meals[indexPath.row].name
            })
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Calories"
                textField.text = String(self.meals[indexPath.row].calories)
            })
            
            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (updateAction) in
                let name = alert.textFields!.first!.text!
                
                guard let calories = Int32(alert.textFields![1].text!) else {
                    self.showAlert(message: "Please put a valid number for this meal's calories")
                    return
                }
                self.meals[indexPath.row].name = name
                self.meals[indexPath.row].calories = calories
                APIClient.Update(meal:self.meals[indexPath.row]) { result in
                    switch result {
                    case .success(let mealResponse):
                        self.meals[indexPath.row] = mealResponse.meal
                        self.tableView.reloadData()
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }))
            self.present(alert, animated: false)
        })
        editAction.backgroundColor = UIColor.orange
        
        let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
            
            let m = self.meals[indexPath.row]
            APIClient.Delete(id: m.id!) { result in
                switch result {
                case .success(let mealResponse):
                    self.meals.remove(at: indexPath.row)
                    self.tableView.reloadData()
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            
        })
        
        return [deleteAction, editAction]
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation
 

}
