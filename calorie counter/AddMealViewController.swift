//
//  AddMealViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/18/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class AddMealViewController: UIViewController {

    @IBOutlet var calorieField: UITextField!
    @IBOutlet var nameField: UITextField!
    
    var meal : Meal?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        guard let calorieText = calorieField.text else {
            print("Can't have null value for calories")
            return
        }
        
        guard let name = nameField.text else {
            print("Can't have null value for calories")
            return
        }
        
        guard let calories = Int32(calorieText) else {
            print("must enter a valid integer")
            return
        }
        
        let callback: (_ meal: Meal?, _ error: Error?) -> Void = { (meal, error) -> Void in
            guard error == nil else {
                print("Meal failed to save")
                return
            }
            DispatchQueue.main.async {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        let meal = Meal(name: name, calories: calories)
        
        APIClient.Create(meal: meal){ result in
            switch result {
            case .success(let meal):
                DispatchQueue.main.async {
                    self.dismiss(animated: true, completion: nil)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        /*
        if let meal = meal {
            meal.update(completionHandler: callback)
            
        } else {
            meal = Meal(name: name, calories: calories)
            meal!.save(completionHandler: callback)
        }
        */
        
        

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
