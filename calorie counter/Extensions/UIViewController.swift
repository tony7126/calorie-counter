//
//  UIViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/23/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(message: String, title: String="Error") {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
