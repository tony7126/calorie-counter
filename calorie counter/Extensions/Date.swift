//
//  Date.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/20/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Foundation

extension Date {
    
    func stripTime() -> Date {
        let components = Calendar.current.dateComponents([.year, .month, .day], from: self)
        let date = Calendar.current.date(from: components)
        return date!
    }
    
}

extension DateFormatter {
    static var articleDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
}
