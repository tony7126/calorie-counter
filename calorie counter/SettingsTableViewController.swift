//
//  SettingsTableViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    var canShowUserOption = false
    var calTarget : Int = 0
    var user : User?
    @IBOutlet var calorieTableCell: UITableViewCell!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        APIClient.GetCurrentUser(){ result in
            switch result {
            case .success(let userResponse):
                self.user = userResponse.user
                
                let roles = ["Admin", "User Manager"]
                self.calTarget = userResponse.user.calorieTarget
                self.calorieTableCell.textLabel?.text = String(self.calTarget)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                if roles.contains(userResponse.user.role) {
                    self.canShowUserOption = true
                    self.tableView.reloadData()
                }
                return
            case .failure(let error):
                self.showAlert(message: "Error loading user settings")
                print(error.localizedDescription)
                return
            }
        }
        
        if let userId = User.GetUserMimicked() {
            let title = self.navigationItem.title
            //self.navigationItem.title = title! + " (Mimicked: \(userId))"
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if cell?.tag == 5 {
            let refreshAlert = UIAlertController(title: "Stop Mimicking User", message: "Are you sure you want to stop mimicking user?", preferredStyle: UIAlertController.Style.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                User.RemoveUserMimickery()
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default))
            
            present(refreshAlert, animated: true, completion: nil)
        }
        else if cell?.tag == 6 {
            User.logout(){ isLoggedOut, error in
                if isLoggedOut {
                    DispatchQueue.main.async {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.updateRootVC()
                    }
                }
                
            }

        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        guard indexPath.row == 0 && indexPath.section == 0 else {
            
            return nil
        }
        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
            let alert = UIAlertController(title: "", message: "Adjust Calorie Count", preferredStyle: .alert)
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Target Calories"
                textField.text = String(self.calTarget)
            })
            
            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (updateAction) in
                let newCalTarget = Int(alert.textFields!.first!.text!)
                var updateUserRequest = UpdateUserRequest()
                guard let nct = newCalTarget, nct != self.user!.calorieTarget else {
                    return
                }
                updateUserRequest.calorieTarget = nct
                APIClient.UpdateUser(user: self.user!, updates: updateUserRequest, useMimic: false) { result in
                    switch result {
                    case .success(let userResponse):
                        self.user = userResponse.user
                        self.calorieTableCell.textLabel?.text = String(self.user!.calorieTarget)
                        self.tableView.reloadData()
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }

            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: false)
        })
        
        editAction.backgroundColor = UIColor.orange
        
        return [editAction]
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section != 1 || canShowUserOption {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
        return 0
    }

    // MARK: - Table view data source
    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
