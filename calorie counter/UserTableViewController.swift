//
//  UserTableViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class UserTableViewController: UITableViewController {
    var users : [User] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        APIClient.FetchUsers() { result in
            
            switch result {
            case .success(let usersResponse):
                self.users = []
                for userResponse in usersResponse.users{
                    self.users.append(userResponse.user)
                }
                self.tableView.reloadData()
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)

        // Configure the cell...
        cell.textLabel?.text = users[indexPath.row].username

        return cell
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: .default, title: "Edit", handler: { (action, indexPath) in
            let alert = UIAlertController(title: "", message: "Edit User", preferredStyle: .alert)
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Username"
                textField.text = self.users[indexPath.row].username
            })
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "New Password"
            })
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.text =  String(self.users[indexPath.row].calorieTarget)
                textField.placeholder = "Calorie Target"
            })
            
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Email"
                textField.text = self.users[indexPath.row].email
            })
            
            alert.addAction(UIAlertAction(title: "Update", style: .default, handler: { (updateAction) in
                let newUsername = alert.textFields!.first!.text!
                let newPassword = alert.textFields![1].text!
                let calTarget = Int(alert.textFields![2].text!)
                let newEmail = alert.textFields![3].text!
                let u = self.users[indexPath.row]
                var userUpdateRequest = UpdateUserRequest()
                if u.calorieTarget != calTarget {
                    userUpdateRequest.calorieTarget = calTarget
                }
                
                if !newPassword.isEmpty {
                    userUpdateRequest.password = newPassword
                }
                
                if u.username != newUsername {
                    userUpdateRequest.username = newUsername
                }
                
                if u.email != newEmail {
                    userUpdateRequest.email = newEmail
                }
                
                APIClient.UpdateUser(user: u,updates: userUpdateRequest, useMimic: false) { result in
                    switch result {
                    case .success(let userResponse):
                        self.users[indexPath.row] = userResponse.user
                        self.tableView.reloadData()
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
                
                
                self.users[indexPath.row].username = alert.textFields!.first!.text!
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: false)
        })
        
            editAction.backgroundColor = UIColor.orange
        
            let deleteAction = UITableViewRowAction(style: .default, title: "Delete", handler: { (action, indexPath) in
                
            let u = self.users[indexPath.row]
            
            APIClient.DeleteUser(id: u.id!) { result in
                switch result {
                case .success(_):
                    self.users.remove(at: indexPath.row)
                    DispatchQueue.main.async {
                        tableView.reloadData()
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
            }
            
        })
        
        return [deleteAction, editAction]
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = users[indexPath.row]
        let refreshAlert = UIAlertController(title: "Mimic User", message: "Are you sure you want to change user?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            User.AddUserMimickery(user: user)
        }))
        
         refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .default))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
