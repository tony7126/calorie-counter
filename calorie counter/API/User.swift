//
//  User.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/19/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

class Token {
    
}

struct UsersResponse : Decodable {
    let users: [UserResponse]
    enum MealResponseKey: CodingKey {
        case user
    }
}

struct UpdateUserRequest {
    var calorieTarget : Int?
    var username : String?
    var password : String?
    var email : String?
    
    func jsonify() -> [String: Any]{
        var params : [String: Any] = [:]
        if let calorieTarget = calorieTarget{
            params["daily_calories"] = calorieTarget
        }
        if let email = email{
            params["email"] = email
        }
        if let username = username{
            params["username"] = username
        }
        if let password = password{
            params["password"] = password
        }
        return params
    }
}

struct UserResponse : Decodable {
    let user: User
    enum UserResponseKey: CodingKey {
        case user
    }
}

class User : Decodable{
    var username: String
    var id : Int?
    var role: String
    var calorieTarget: Int
    var email: String
    var password : String?
    
    init(username: String, calorieTarget: Int, email: String, password: String) {
        self.username = username
        self.calorieTarget = calorieTarget
        self.email = email
        self.password = password
        self.role = "User"
    }
    
    private enum CodingKeys: String, CodingKey {
        case username
        case password
        case id
        case calorie_target
        case email
        case role
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        username = try values.decode(String.self, forKey: .username)
        email = try values.decode(String.self, forKey: .email)
        calorieTarget = try values.decode(Int.self, forKey: .calorie_target)
        id = try values.decode(Int.self, forKey: .id)
        self.role = try values.decode(String.self, forKey: .role)
    }
    
    class func SaveUserInfo(username: String, password: String, userId: Int)->Bool {
        var saveSuccessful = KeychainWrapper.standard.set(username, forKey: "username")
        guard saveSuccessful else {
            return false
        }
    
        saveSuccessful = KeychainWrapper.standard.set(password, forKey: "password")
        guard saveSuccessful else {
            return false
        }
    
        UserDefaults.standard.set(userId, forKey: "userId")
        return true
    }
    
    class func IsLoggedIn()->Bool{
        let (username, password) = getCreds()
        guard let _ = username, let _ = password else {
            return false
        }
        
        return true
        
    }
    
    class func logout(completionHandler:@escaping (_ success: Bool, _ error: Error?) -> Void){
        KeychainWrapper.standard.removeObject(forKey: "username")
        KeychainWrapper.standard.removeObject(forKey: "password")
        UserDefaults.standard.removeObject(forKey: "userId")
        UserDefaults.standard.removeObject(forKey: Constants.mimickedUserKey)
        completionHandler(true, nil)
    }
    
    class func getCreds()->(String?, String?){
        let username: String? = KeychainWrapper.standard.string(forKey: "username")
        let password: String? = KeychainWrapper.standard.string(forKey: "password")
        return (username, password)
    }
    
    class func AddUserMimickery(user: User){
        UserDefaults.standard.set(user.id, forKey: Constants.mimickedUserKey)
    }
    
    class func GetUserMimicked()->Int?{
        return UserDefaults.standard.object(forKey: Constants.mimickedUserKey) as? Int
    }
    
    class func RemoveUserMimickery(){
        UserDefaults.standard.removeObject(forKey: Constants.mimickedUserKey)
    }
    
}
