//
//  UserRouter.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Alamofire

enum UserEndpoint: APIConfiguration {
    
    case Create(user: User, useAuth: Bool)
    case Update(user: User, updates: UpdateUserRequest, useMimic: Bool)
    case Fetch
    case Get(id: Int)
    case Delete(id: Int)
    case Login(username: String, password: String)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .Create:
            return .post
        case .Delete:
            return .delete
        case .Update:
            return .put
        case .Fetch:
            return .get
        case .Get:
            return .get
        case .Login:
            return .post
            
        }
    }
    
    var useAuth: Bool {
        switch self {
        case .Create(_, let useAuth):
            return useAuth
        case .Update(_, _, let useAuth):
            return useAuth
        case .Login:
            return false
        default:
            return true
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .Get(let id):
            return "/api/user/\(id)"
        case .Update(let user, _, _):
            return "/api/user/\(user.id!)"
        case .Create:
            return "/api/user"
        case .Fetch:
            return "/api/user"
        case .Delete(let id):
            return "/api/user/\(id)"
        case .Login:
            return "/api/user/login"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .Update(_, let updates, _):
            return updates.jsonify()
        case .Create(let user, _):
            return ["username": user.username, "daily_calories": user.calorieTarget, "email": user.email, "password": user.password!] as [String : Any]
        case .Login(let username, let password):
            return ["username": username, "password": password]
        default:
            return nil
        }
    }
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try K.ProductionServer.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        if useAuth {
            let (username, password) = User.getCreds()
            guard let u = username, let p = password else {
                print("Unable to retreive username/password")
                throw InvalidCredentialsError.noValidCredentials
            }
            let credentialData = "\(u):\(p)".data(using: .utf8)
            guard let cred = credentialData else { throw InvalidCredentialsError.noValidCredentials}
            let base64Credentials = cred.base64EncodedData(options: [])
            guard let base64Date = Data(base64Encoded: base64Credentials) else { throw InvalidCredentialsError.noValidCredentials }
            urlRequest.setValue("Basic \(base64Date.base64EncodedString())", forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
            urlRequest.url = urlRequest.url!.append("username", value: u)
            urlRequest.url = urlRequest.url!.append("password",  value: p)
        }
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        if let userMimickedId = User.GetUserMimicked() {
            urlRequest.url = urlRequest.url!.append("user-mimicked", value: String(userMimickedId))
        }
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
