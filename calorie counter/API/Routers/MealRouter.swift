//
//  APIRouter.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//
import Alamofire

enum MealEndpoint: APIConfiguration {
    
    case Create(meal: Meal)
    case Update(meal: Meal)
    case Fetch(sts: String?, ets: String?, sds: String?, eds: String?)
    case Get(id: Int)
    case Delete(id: Int)
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .Create:
            return .post
        case .Delete:
            return .delete
        case .Update:
            return .put
        case .Fetch:
            return .get
        case .Get:
            return .get
        
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
        case .Get(let id):
            return "/api/meal/\(id)"
        case .Update(let meal):
            return "/api/meal/\(meal.id!)"
        case .Create:
            return "/api/meal"
        case .Fetch:
            return "/api/meal"
        case .Delete(let id):
            return "/api/meal/\(id)"
        }
    }
    
    // MARK: - Parameters
    var parameters: Parameters? {
        switch self {
        case .Update(let meal):
            return ["calories": meal.calories, "name": meal.name, "username": "tony", "password": "pass1"] as [String : Any]
        case .Create(let meal):
            return ["calories": meal.calories, "name": meal.name, "date": meal.datetime.timeIntervalSince1970, "username": "tony", "password": "pass1"] as [String : Any]
        default:
            return nil
        }
    }
    
    var urlQueryParameters : [String: String]? {
        switch self {
        case .Fetch(let sts, let ets, let sds, let eds):
            var params : [String: String] = [:]
            
            if let sts = sts { params["start_time"] = sts }
            if let ets = ets { params["end_time"] = ets }
            if let sds = sds { params["start_date"] = sds }
            if let eds = eds { params["end_date"] = eds }
            
            return params
        default:
            return nil
        }
    }
    
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let (username, password) = User.getCreds()
        guard let u = username, let p = password else {
            print("Unable to retreive username/password")
            throw InvalidCredentialsError.noValidCredentials
        }
        let url = try K.ProductionServer.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        // Common Headers
        
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        let credentialData = "\(u):\(p)".data(using: .utf8)
        guard let cred = credentialData else { throw InvalidCredentialsError.noValidCredentials}
        let base64Credentials = cred.base64EncodedData(options: [])
        guard let base64Date = Data(base64Encoded: base64Credentials) else { throw InvalidCredentialsError.noValidCredentials }
        urlRequest.setValue("Basic \(base64Date.base64EncodedString())", forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
        
        if let userMimickedId = User.GetUserMimicked() {
            urlRequest.url = urlRequest.url!.append("user-mimicked", value: String(userMimickedId))
        }
        
        // Parameters
        
        urlRequest.url = urlRequest.url!.append("username", value: u)
        urlRequest.url = urlRequest.url!.append("password",  value: p)
        
        if let urlParams = urlQueryParameters {
            for (k, v) in urlParams {
                urlRequest.url = urlRequest.url!.append(k, value: v)
            }
        }

        if let parameters = parameters {
            do {
                
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
extension URL {
    
    @discardableResult
    func append(_ queryItem: String, value: String?) -> URL {
        
        guard var urlComponents = URLComponents(string:  absoluteString) else { return absoluteURL }
        
        // create array of existing query items
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        
        // create query item if value is not nil
        guard let value = value else { return absoluteURL }
        let queryItem = URLQueryItem(name: queryItem, value: value)
        
        // append the new query item in the existing query items array
        queryItems.append(queryItem)
        
        // append updated query items array in the url component object
        urlComponents.queryItems = queryItems// queryItems?.append(item)
        
        // returns the url from new url components
        return urlComponents.url!
    }
}
