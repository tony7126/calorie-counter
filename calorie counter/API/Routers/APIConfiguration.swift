//
//  APIConfiguration.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Alamofire

protocol APIConfiguration: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
}
