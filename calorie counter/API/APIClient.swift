//
//  APIClient.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Alamofire

class APIClient {
    static func Create(meal: Meal, completion:@escaping (Result<MealResponse>)->Void) {
        AF.request( MealEndpoint.Create(meal: meal))
            .responseDecodable { (response: DataResponse<MealResponse>) in
                completion(response.result)
        }
    }
    
    static func Fetch(sts: String?=nil, ets: String?=nil, sds: String?=nil, eds: String?=nil, completion:@escaping (Result<MealsResponse>)->Void) {
        AF.request( MealEndpoint.Fetch(sts: sts, ets: ets, sds: sds, eds: eds))
            .responseDecodable { (response: DataResponse<MealsResponse>) in
                completion(response.result)
        }
    }
    
    static func Update(meal: Meal, completion:@escaping (Result<MealResponse>)->Void) {
        AF.request( MealEndpoint.Update(meal: meal))
            .responseDecodable { (response: DataResponse<MealResponse>) in
                completion(response.result)
                
        }
    }
    
    static func Get(id: Int, completion:@escaping (Result<MealResponse>)->Void) {
        AF.request( MealEndpoint.Get(id: id))
            .responseDecodable { (response: DataResponse<MealResponse>) in
                completion(response.result)
                
        }
    }
    
    static func Delete(id: Int, completion:@escaping (Result<MealResponse>)->Void) {
        AF.request( MealEndpoint.Delete(id: id))
            .responseDecodable { (response: DataResponse<MealResponse>) in
                completion(response.result)
                
        }
    }
    
    static func CreateUser(user: User, completion:@escaping (Result<UserResponse>)->Void) {
        AF.request( UserEndpoint.Create(user: user, useAuth: false))
            .responseDecodable { (response: DataResponse<UserResponse>) in
                completion(response.result)
        }
    }
    
    static func GetCurrentUser(completion:@escaping (Result<UserResponse>)->Void) {
        let userId = UserDefaults.standard.integer(forKey: "userId")
        AF.request( UserEndpoint.Get(id: userId))
            .responseDecodable { (response: DataResponse<UserResponse>) in
                completion(response.result)
        }
    }
    
    
    static func FetchUsers(completion:@escaping (Result<UsersResponse>)->Void) {
        AF.request( UserEndpoint.Fetch)
            .responseDecodable { (response: DataResponse<UsersResponse>) in
                completion(response.result)
        }
    }
    
    static func UpdateUser(user: User, updates: UpdateUserRequest, useMimic: Bool, completion:@escaping (Result<UserResponse>)->Void) {
        AF.request( UserEndpoint.Update(user: user, updates: updates, useMimic: useMimic))
            .responseDecodable { (response: DataResponse<UserResponse>) in
                completion(response.result)
                
        }
    }
    
    static func DeleteUser(id: Int, completion:@escaping (Result<UserResponse>)->Void) {
        AF.request( UserEndpoint.Delete(id: id))
            .responseDecodable { (response: DataResponse<UserResponse>) in
                completion(response.result)
                
        }
    }
    
    static func Login(username: String, password: String, completion:@escaping (Result<UserResponse>)->Void) {
        AF.request( UserEndpoint.Login(username: username, password: password))
            .responseDecodable { (response: DataResponse<UserResponse>) in
                completion(response.result)
                
        }
    }
    
}
