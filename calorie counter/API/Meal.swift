//
//  Meal.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/18/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

struct MealsResponse : Decodable {
    let meals: [MealResponse]
    let calorie_target : Int
    enum MealResponseKey: CodingKey {
        case meals
    }
}

struct MealResponse : Decodable {
    let meal: Meal
    enum MealResponseKey: CodingKey {
        case meal
    }
}

class Meal : Codable {
    var name: String
    var calories : Int32
    var datetime : Date
    var id : Int?
    
    init(name: String, calories: Int32){
        self.name = name
        self.calories = calories
        self.datetime = Date()
    }
    
    private enum CodingKeys: String, CodingKey {
        case name
        case calories
        case id
        case time
        case date
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        id = try values.decode(Int.self, forKey: .id)
        calories = Int32(try values.decode(Int.self, forKey: .calories))
        let timeString = try values.decode(String.self, forKey: .time)
        let dateString = try values.decode(String.self, forKey: .date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        let date = dateFormatter.date(from: "\(dateString) \(timeString)")
        self.datetime = date!
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(id, forKey: .id)
        try container.encode(calories, forKey: .calories)
    }
    
    class func Get(){
        
    }
    
    func delete(){
        
    }
    
}
