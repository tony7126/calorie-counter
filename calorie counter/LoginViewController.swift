//
//  LoginViewController.swift
//  Alamofire
//
//  Created by Tony Lambropoulos on 3/19/19.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var usernameField: UITextField!
    
    var origConstraint : CGFloat = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        origConstraint = bottomConstraint.constant
        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            // Get keyboard frame
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            // Set new bottom constraint constant
            var bottomConstraintConstant = keyboardFrame.origin.y >= UIScreen.main.bounds.size.height ? 0.0 : keyboardFrame.size.height
            
            // Set animation properties
            let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)
            if bottomConstraintConstant == 0 {
                bottomConstraintConstant = origConstraint
            }
            // Animate the view you care about
            UIView.animate(withDuration: duration, delay: 0, options: animationCurve, animations: {
                self.bottomConstraint.constant = bottomConstraintConstant
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @IBAction func login(_ sender: UIButton) {
        guard let passwordText = passwordField.text else {
            return
        }
        
        guard let usernameText = usernameField.text else {
            return
        }
        
        APIClient.Login(username: usernameText, password: passwordText) { result in
            switch result {
            case.success(let userResponse):
                guard User.SaveUserInfo(username: usernameText, password: passwordText, userId: userResponse.user.id!) else {
                    print("error saving user credentials")
                    self.showAlert(message: "There was an error logging in, please try again")
                    return
                }
                
                DispatchQueue.main.async {
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.updateRootVC()
                }
                
            case.failure(let error):
                self.showAlert(message: "There was an error logging in, please try again")
                print("Error logging in \(error)")
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
