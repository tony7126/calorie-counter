//
//  CreateUserViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class CreateUserViewController: UIViewController {
    
    @IBOutlet var calorieTargetField: UITextField!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var confirmPasswordField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var emailField: UITextField!
    var origConstraint : CGFloat = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Subscribe to keyboard notifications
        origConstraint = bottomConstraint.constant
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardNotification(notification:)),
                                               name: UIResponder.keyboardWillChangeFrameNotification,
                                               object: nil)
        // Do any additional setup after loading the view.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            // Get keyboard frame
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            // Set new bottom constraint constant
            var bottomConstraintConstant = keyboardFrame.origin.y >= UIScreen.main.bounds.size.height ? 0.0 : keyboardFrame.size.height
            
            // Set animation properties
            let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIView.AnimationOptions.curveEaseInOut.rawValue
            let animationCurve = UIView.AnimationOptions(rawValue: animationCurveRaw)
            
            // Animate the view you care about
            if bottomConstraintConstant == 0 {
                bottomConstraintConstant = origConstraint
            }
            UIView.animate(withDuration: duration, delay: 0, options: animationCurve, animations: {
                self.bottomConstraint.constant = bottomConstraintConstant
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        guard let passwordText = passwordField.text, passwordText.count > 7 else {
            self.showAlert(message: "Please enter a password that is greater than 7 characters")
            return
        }
        
        guard let usernameText = usernameField.text else {
            self.showAlert(message: "Please enter an username")
            return
        }
        
        guard let emailText = emailField.text else {
            self.showAlert(message: "Please enter an email address")
            return
        }
        
        guard let confirmPasswordText = confirmPasswordField.text else {
            self.showAlert(message: "Please confirm your password")
            return
        }
        
        guard let calorieTargetText = calorieTargetField.text else {
            self.showAlert(message: "Please enter a calorie target")
            return
        }
        
        guard passwordText == confirmPasswordText else {
            self.showAlert(message: "Please ensure your passwords match")
            return
        }
        
        guard let calorieTarget = Int(calorieTargetText) else {
            self.showAlert(message: "Please put a valid number for your calorie target.")
            return
        }
        
        let user = User(username: usernameText, calorieTarget: calorieTarget, email: emailText, password: passwordText)
        APIClient.CreateUser(user: user) {result in
            switch result {
            case .success(let userResponse):
                let alert = UIAlertController(title: "Account Created!", message: "Congratulations, your account has been created!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            case .failure(let error):
                self.showAlert(message: "Error creating user.")
                print(error.localizedDescription)
            }
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
