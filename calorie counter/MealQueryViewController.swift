//
//  MealQueryViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class MealQueryViewController: UIViewController {

    @IBOutlet var endTimeToggle: UISwitch!
    @IBOutlet var startTimeToggle: UISwitch!
    @IBOutlet var endDateToggle: UISwitch!
    @IBOutlet var startDateToggle: UISwitch!
    @IBOutlet var endTimePicker: UIDatePicker!
    @IBOutlet var endDatePicker: UIDatePicker!
    @IBOutlet var startDatePicker: UIDatePicker!
    @IBOutlet var startTimePicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let userId = User.GetUserMimicked() {
            let title = self.navigationItem.title
            self.navigationItem.title = title! + " (Mimicked: \(userId))"
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let controller = segue.destination as? MealTableTableViewController {
            
            let startDate = startDatePicker.date
            let endDate = endDatePicker.date
            let startTime = startTimePicker.date
            let endTime = endTimePicker.date
            
            let timeFormatter = DateFormatter()
            let dateFormatter = DateFormatter()
            timeFormatter.dateFormat = "HH:mm"
            dateFormatter.dateFormat = "MM/dd/yyyy"
            controller.startDateString = startDateToggle.isOn ?  dateFormatter.string(from: startDate) : nil
            controller.startTimeString = startTimeToggle.isOn ? timeFormatter.string(from: startTime) : nil
            controller.endDateString = endDateToggle.isOn ? dateFormatter.string(from: endDate) :  nil
            controller.endTimeString = endTimeToggle.isOn ? timeFormatter.string(from: endTime) :  nil
        }
    }
    

}
