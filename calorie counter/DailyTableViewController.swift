//
//  DailyTableViewController.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/20/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import UIKit

class DailyTableViewController: UITableViewController {
    var mealListByDay : [(key: Date, value: [Meal])] = []
    var calorieTarget : Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        tableView.prefetchDataSource = self
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        aggregateMealsByDay()
    }
    
    func aggregateMealsByDay(){
        APIClient.Fetch(){ result in
            
            switch result {
            case .success(let mealsResponse):
                var meals : [Meal] = []
                for mealResponse in mealsResponse.meals{
                    meals.append(mealResponse.meal)
                }
                
                self.calorieTarget = mealsResponse.calorie_target
                
                self.mealListByDay = Dictionary(grouping: meals, by: { $0.datetime.stripTime() }).sorted {$0.key > $1.key}
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.mealListByDay.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dailySummaryCell", for: indexPath)

        // Configure the cell...
        let date = self.mealListByDay[indexPath.row].key
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        cell.textLabel?.text = dateFormatter.string(from: date)
        let meals = self.mealListByDay[indexPath.row].value as [Meal]
        var totCals : Int32 = 0
        for meal in meals {totCals += meal.calories}
        cell.detailTextLabel?.text = "Total Calories: " + String(totCals)
        if totCals > self.calorieTarget {
            cell.backgroundColor = UIColor.red
        } else {
            cell.backgroundColor = UIColor.green
        }
        return cell
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DailyTableViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
    }
}
