//
//  Constants.swift
//  calorie counter
//
//  Created by Tony Lambropoulos on 3/21/19.
//  Copyright © 2019 Toptal. All rights reserved.
//

import Foundation

class Constants {
    static let mimickedUserKey = "mimickedUserKey"
}

struct K {
    struct ProductionServer {
        static let baseURL = "https://zjs8yw2vf9.execute-api.us-east-1.amazonaws.com/dev"//"http://localhost:5000"
    }
    
    struct APIParameterKey {
        static let password = "password"
        static let username = "username"
    }
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

enum InvalidCredentialsError: Error {
    case noValidCredentials
}
