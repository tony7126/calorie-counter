from jsonschema import Draft6Validator
from functools import wraps
from jsonschema.exceptions import ValidationError
from flask import g, request, redirect, url_for, abort, jsonify


def json_validator(val_object):
    def json_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                print(request.json)
                val_object.validate(request.json)
            except ValidationError as ve:
                print(ve.message)
                return jsonify({"error": ve.message}), 400
            return f(*args, **kwargs)
        return wrapper
    return json_decorator

def query_string_validator(val_object):
    def json_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            try:
                val_object.validate(request.args)
            except ValidationError as ve:
                return {"error": ve.message}, 400
            return f(*args, **kwargs)
        return wrapper
    return json_decorator 

list_meals_validator = Draft6Validator({
    'type': 'object',
    'properties': {
        'start_time': {'type': 'string'},
        'end_time': {'type': 'string'},
        'start_date': {'type': 'string'},
        'end_date': {'type': 'string'},
        'username': {'type': 'string'},
        'password': {'type': 'string'},
    },
    'required': ['username', 'password']
})

update_user_validator = Draft6Validator({
    'type': 'object',
    'properties': {
        'username': {'type': 'string'},
        'password': {'type': 'string'},
        'email': {'type': 'string'},
        'daily_calories': {'type': 'integer'},
    },
    'required': []
})

login_validator = Draft6Validator({
    'type': 'object',
    'properties': {
        'username': {'type': 'string'},
        'password': {'type': 'string'},

    },
    'required': ['username', 'password']
})

create_user_validator = Draft6Validator({
    'type': 'object',
    'properties': {
        'username': {'type': 'string'},
        'password': {'type': 'string'},
        'email': {'type': 'string'},
        'daily_calories': {'type': 'integer'},
    },
    'required': ['username', 'password', 'email', 'daily_calories']
})

update_meal_validator = Draft6Validator({
    'type': 'object',
    'properties': {
        'name': {'type': 'string'},
        'calories': {'type': 'integer'},
    },
})