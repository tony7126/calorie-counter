from app import app, db
from models import User, Meal, Role
from flask_httpauth import HTTPBasicAuth
from flask import request, jsonify, abort, url_for, g
from auth import has_role, role_user_switch
import datetime
from sqlalchemy import and_
from validators import (json_validator,
                        query_string_validator,
                        update_user_validator, 
                        login_validator, 
                        create_user_validator, 
                        update_meal_validator,
                        list_meals_validator)


class HTTPBasicAuthMod(HTTPBasicAuth):

    def authenticate(self, auth, stored_password):
        username = request.args["username"]
        client_password = request.args["password"]
        if self.verify_password_callback:
            return self.verify_password_callback(username, client_password)
        if not auth:
            return False
        if self.hash_password_callback:
            try:
                client_password = self.hash_password_callback(client_password)
            except TypeError:
                client_password = self.hash_password_callback(username,
                                                              client_password)
        return client_password is not None and \
            client_password == stored_password

auth = HTTPBasicAuthMod()

@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user or not user.verify_password(password):
            return False
    g.user = user
    print("verifypassword", g.user.username)
    return True

@app.route("/api/user/login", methods=["POST"])
@json_validator(login_validator)
def login():
    username = request.json['username']
    password = request.json['password']
    user = User.query.filter_by(username=username).first()
    if user.verify_password(password):
        return jsonify({"user": user.to_dict()})
    else:
        abort(401)

@app.route('/api/user', methods=['POST'])
@json_validator(create_user_validator)
def new_user():
    username = request.json['username']
    password = request.json['password']
    if User.query.filter_by(username=username).first() is not None:
        abort(400)    # existing user

    role = Role.query.filter_by(name="User").first()
    user = User(username=username, email=request.json["email"], daily_calories=request.json["daily_calories"], role=role)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return (jsonify({'user': user.to_dict()}), 201,
            {'Location': url_for('get_user', id=user.id, _external=True)})

@app.route('/api/user', methods=['GET'])
@auth.login_required
@has_role(["Admin", "UserManager"])
def list_users():
    return jsonify({"users": [{"user": user.to_dict()} for user in User.query.all()]})

@app.route("/api/user/<int:id>", methods=["PUT"])
@role_user_switch(["Admin", "User Manager"])
@json_validator(update_user_validator)
def update_user(id):
    user = User.query.filter_by(id=id).first()

    for key, value in request.json.items():
        if key == "password":
            user.hash_password(value)
        else:
            setattr(user, key, value)

    db.session.add(user)
    db.session.commit()
    return jsonify({"user": user.to_dict()})

@app.route('/api/user/<int:id>')
@auth.login_required
@role_user_switch(["Admin", "User Manager"])
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return jsonify({'user': user.to_dict()})


@app.route('/api/user/<int:id>', methods=["DELETE",])
@auth.login_required
def delete_user(id):
    if g.user.id != id and not (g.user.has_role("Admin") or g.user.hash_role("User Manager")):
        abort(404)
    user = User.query.get(id)

    if not user:
        abort(400)

    db.session.delete(user)
    db.session.commit()
    return jsonify({'user': user.to_dict()})

@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@app.route('/api/meal', methods=["POST"])
@auth.login_required
@role_user_switch(["Admin"])
def create_meal():
    print(g.user.id)
    name = request.json.get('name')
    calories = request.json.get('calories')
    date_since_1970 = request.json.get('date')
    meal_time = datetime.datetime.fromtimestamp(date_since_1970)

    meal = Meal(name=name, calories=calories, user=g.user, date=meal_time.date(), time=meal_time.time())

    db.session.add(meal)
    db.session.commit()

    return jsonify({"status": "success", "meal": meal.to_dict()})


@app.route('/api/meal/<int:id>', methods=["DELETE"])
@auth.login_required
@role_user_switch(["Admin"])
def delete_meal(id):
    meal = Meal.query.filter_by(user_id=g.user.id, id=id).first()
    if not meal:
        abort(400)
    
    db.session.delete(meal)
    db.session.commit()
    return jsonify({"meal": meal.to_dict()})

@app.route('/api/meal/<int:id>', methods=["PUT"])
@auth.login_required
@role_user_switch(["Admin"])
@json_validator(update_meal_validator)
def update_meal(id):
    name = request.json.get('name')
    calories = request.json.get('calories')
    meal = Meal.query.filter_by(id=id).first()
    meal.name = name
    meal.calories = calories
    db.session.add(meal)
    db.session.commit()

    return jsonify({"meal": meal.to_dict()})

@app.route('/api/meal', methods=["GET"])
@auth.login_required
@role_user_switch(["Admin"])
@query_string_validator(list_meals_validator)
def list_meals():
    print("user_id", g.user.id)
    query = Meal.query.filter_by(user_id=g.user.id)

    if "start_time" in request.args:
        sti = datetime.datetime.strptime(request.args["start_time"], '%H:%M').time()
        print(sti)
        query = query.filter(Meal.time >= sti)

    if "end_time" in request.args:
        eti = datetime.datetime.strptime(request.args["end_time"], '%H:%M').time()
        print(eti)
        query = query.filter(Meal.time <= eti)

    if "start_date" in request.args:
        sdi = datetime.datetime.strptime(request.args["start_date"], '%m/%d/%Y').date()
        print(sdi)
        query = query.filter(Meal.date >= sdi)

    if "end_date" in request.args:
        edi = datetime.datetime.strptime(request.args["end_date"], '%m/%d/%Y').date()
        print(edi)
        query = query.filter(Meal.date <= edi)
    
    l = [{"meal": meal.to_dict()} for meal in query]

    return jsonify({"meals": l, "calorie_target": g.user.daily_calories})

