import os
from app import app
from api import *
# --------- Server On ----------
# start the webserver
if __name__ == "__main__":
	port = int(os.environ.get('PORT', 5000)) # locally PORT 5000, Heroku will assign its own port
	app.run(host='0.0.0.0', port=port)