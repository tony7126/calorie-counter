from functools import wraps
from flask import g, request, redirect, url_for, abort, jsonify
from models import User

def has_role(roles):
    def has_role_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if g.user.id != id and not (g.user.has_role("Admin") or g.user.has_role("User Manager")):
                abort(401)
            return f(*args, **kwargs)
        return wrapper
    return has_role_decorator


def role_user_switch(allowed_roles):
    def has_role_decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            if "user-mimicked" in request.args and g.user.id != request.args["user-mimicked"]:
                user_id = request.args["user-mimicked"]
                if g.user.role.name in allowed_roles:
                    g.user = User.query.filter_by(id=user_id).first()
                    print(g.user.username)
                else:
                    abort(401)
            return f(*args, **kwargs)
        return wrapper
    return has_role_decorator
