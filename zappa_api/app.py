import os
import time
from flask import Flask
import logging
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.engine import reflection
from sqlalchemy.schema import (
        MetaData,
        Table,
        DropTable,
        ForeignKeyConstraint,
        DropConstraint,
        )
app = Flask(__name__)   # create our flask app

if 'SERVERTYPE' in os.environ and os.environ['SERVERTYPE'] == 'AWS Lambda':
    app.config['SQLALCHEMY_DATABASE_URI'] =  os.environ["DATABASE_URL"]
    app.config['SECRET_KEY'] = os.environ["SECRET_KEY"]
else:
    app.config['SECRET_KEY'] = '5gJPeSaqgQ4N3DTWsvxr'
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'

app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(session_options = {
    'expire_on_commit': False
})

def drop_db():
    # From http://www.sqlalchemy.org/trac/wiki/UsageRecipes/DropEverything
    with app.app_context():
        conn=db.engine.connect()

        # the transaction only applies if the DB supports
        # transactional DDL, i.e. Postgresql, MS SQL Server
        trans = conn.begin()

        inspector = reflection.Inspector.from_engine(db.engine)

        # gather all data first before dropping anything.
        # some DBs lock after things have been dropped in 
        # a transaction.
        metadata = MetaData()

        tbs = []
        all_fks = []

        for table_name in inspector.get_table_names():
            fks = []
            for fk in inspector.get_foreign_keys(table_name):
                if not fk['name']:
                    continue
                fks.append(
                    ForeignKeyConstraint((),(),name=fk['name'])
                    )
            t = Table(table_name,metadata,*fks)
            tbs.append(t)
            all_fks.extend(fks)

        for fkc in all_fks:
            conn.execute(DropConstraint(fkc))

        for table in tbs:
            conn.execute(DropTable(table))

        trans.commit()

with app.app_context():
    db.init_app(app)
    



PRODUCTION = "PRODUCTION" in os.environ
app.debug = True
if PRODUCTION:
    app.logger.addHandler(logging.StreamHandler())
    app.logger.setLevel(logging.INFO)

