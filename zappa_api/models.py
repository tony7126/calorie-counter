from flask_sqlalchemy import SQLAlchemy
from app import db, app
import bcrypt
from itsdangerous import (TimedJSONWebSignatureSerializer
                          as Serializer, BadSignature, SignatureExpired)
"""
user_permissions_table=db.Table('user_permissions_table', 
                             db.Column('permission_id', db.Integer,db.ForeignKey('permissions.id'), nullable=False),
                             db.Column('user_id',db.Integer,db.ForeignKey('users.id'),nullable=False),
                             db.PrimaryKeyConstraint('permission_id', 'user_id') )
"""

ADMIN = 0
USER_MANAGER = 1
USER = 2

roles = [ADMIN, USER_MANAGER, USER]

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))

class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False)
    role = db.relationship('Role')
    email = db.Column(db.String(64), nullable=False)
    daily_calories = db.Column(db.Integer, default=2000)

    def hash_password(self, password):
        self.password_hash = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).hex()

    def verify_password(self, password):
        phash = bytes.fromhex(self.password_hash)
        return bcrypt.hashpw(password.encode('utf8'), phash) == phash

    def generate_auth_token(self, expiration=600):
        s = Serializer(app.config['SECRET_KEY'], expires_in=expiration)
        return s.dumps({'id': self.id})

    def has_role(self, role):
        return role == self.role.name

    @staticmethod
    def verify_auth_token(token):
        s = Serializer(app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None    # valid token, but expired
        except BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

    def to_dict(self):
        return {"username": self.username,"id": self.id, "role": self.role.name, "calorie_target": self.daily_calories, "email": self.email}


class Meal(db.Model):
    __tablename__ = 'meals'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32))
    calories = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('User')
    date = db.Column(db.Date)
    time = db.Column(db.Time)

    def to_dict(self):
        return {"calories": self.calories, "name": self.name, "id": self.id, "date": '{}'.format(self.date), "time": '{}'.format(self.time)}
