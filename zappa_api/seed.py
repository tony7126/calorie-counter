import app
from models import User, Meal, Role

with app.app.app_context():
    user1 = User(username="usermanager", email="tonyl7126@gmail.com")
    user1.hash_password("pass2")
    print(user1.password_hash)

    role = Role.query.filter_by(name="User Manager").first()
    user1.role = role

    app.db.session.add(user1)

    app.db.session.commit()