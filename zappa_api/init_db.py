from app import app, db
from models import User, Role


def init_db():
    with app.app_context():
        from models import User, Role
        db.create_all()
        r1 = Role(name="Admin")
        r2 = Role(name="User")
        r3 = Role(name="User Manager")

        db.session.add(r1)
        db.session.add(r2)
        db.session.add(r3)

        db.session.commit()

if __name__ == "__main__":
	init_db()