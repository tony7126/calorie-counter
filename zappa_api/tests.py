import unittest
import app
import os
from models import User, Meal
import datetime
import tempfile
import json
from api import (new_user, create_meal)
from base64 import b64encode
from requests.auth import _basic_auth_str


class UserTestCase(unittest.TestCase):
    def setUp(self):
        self.db_fd, self.tempfile = tempfile.mkstemp()
        app.app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + self.tempfile
        app.app.config['TESTING'] = True
        self.client = app.app.test_client()
        with app.app.app_context():
            app.init_db()

    def test_create_user(self):
        ret = self.client.post("/api/user", data = json.dumps({'username': "tony", "password": "test_pass"}), headers={'content-type':'application/json'})
        data = json.loads(ret.data)

        assert data["user"]["username"] == "tony"
        assert "id" in data["user"]

    def test_update_user(self):
        ret = self.client.post("/api/user", data = json.dumps({'username': "tony", "password": "test_pass"}), headers={'content-type':'application/json'})


    def test_delete_user(self):
        pass

    def test_get_user(self):
        pass

    def test_get_users(self):
        pass

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.tempfile)

class MealTestCase(unittest.TestCase):

    def setUp(self):
        self.db_fd, self.tempfile = tempfile.mkstemp()
        app.app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + self.tempfile
        app.app.config['TESTING'] = True
        self.client = app.app.test_client()
        self.now_date = datetime.datetime.now().replace(hour=5, year=2019, day=20, month=5)

        with app.app.app_context():
            app.init_db()
        with app.app.app_context():
            u = User(username="test_user")
            u.hash_password("test_password")
            app.db.session.add(u)
            app.db.session.commit()


    def step1_save_meal(self):
        ret = self.client.post("/api/meal", data = json.dumps({'name': "apple", "calories": 50, "date": int(self.now_date.timestamp())}), headers={'content-type':'application/json', 
            "Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(ret.data)
        assert "meal" in data
        assert data["meal"]["name"] == "apple"
        assert data["meal"]["calories"] == 50
        with app.app.app_context():
            meal = Meal.query.filter_by(id=data["meal"]["id"]).first()
            assert meal

    def step2_success_query_meal(self):
        start_time_string = "03:55"
        end_time_string = "23:59"
        start_date_string = "02/04/2019"
        end_date_string = "12/05/2019"
        response = self.client.get("/api/meal?start_time=%s&end_time=%s&start_date=%s&end_date=%s" % (start_time_string, end_time_string, start_date_string, end_date_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1

    def step3_success_time_query_only(self):
        start_time_string = "03:55"
        end_time_string = "23:59"
        response = self.client.get("/api/meal?start_time=%s&end_time=%s" % (start_time_string, end_time_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1 

        response = self.client.get("/api/meal?start_time=%s" % (start_time_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1 

        response = self.client.get("/api/meal?end_time=%s" % (end_time_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1 

    def step4_success_date_query_only(self):
        start_date_string = "02/04/2019"
        end_date_string = "12/05/2019"
        response = self.client.get("/api/meal?start_date=%s&end_date=%s" % (start_date_string, end_date_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1

        response = self.client.get("/api/meal?start_date=%s" % (start_date_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1

        response = self.client.get("/api/meal?end_date=%s" % (end_date_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 1
        
    def step5_failure_query_out_of_range(self):
        start_time_string = "03:55"
        end_time_string = "23:59"
        start_date_string = "02/04/2017"
        end_date_string = "12/05/2018"

        response = self.client.get("/api/meal?start_time=%s&end_time=%s&start_date=%s&end_date=%s" % (start_time_string, end_time_string, start_date_string, end_date_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 0

        start_time_string = "03:55"
        end_time_string = "04:59"
        response = self.client.get("/api/meal?start_time=%s&end_time=%s&start_date=%s&end_date=%s" % (start_time_string, end_time_string, start_date_string, end_date_string), 
            headers={"Authorization": _basic_auth_str("test_user", "test_password")})
        data = json.loads(response.data)
        assert len(data["meals"]) == 0

    def _steps(self):
        for name in dir(self): # dir() result is implicitly sorted
          if name.startswith("step"):
            yield name, getattr(self, name)

    def test_steps(self):
        for name, step in self._steps():
            try:
                step()
            except Exception as e:
                self.fail("{} failed ({}: {})".format(step, type(e), e))

    def tearDown(self):
        os.close(self.db_fd)
        os.unlink(self.tempfile)



if __name__ == "__main__":
    unittest.main()